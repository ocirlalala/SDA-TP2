# Tugas Pemrograman 2 : Pahlawan dan Dungeon

CSGE602040 - Struktur Data & Algoritma (Data Structures & Algorithms) @ Fakultas Ilmu
Komputer Universitas Indonesia, Semester Gasal 2017/2018


***

## Ketentuan Program

Nama berkas kode sumber		: SDANPMT2.java
Batas waktu eksekusi program: 3 detik / kasus uji
Batas memori program		: 256 MB / kasus uji

## Dunia Pahlawan

Dunia pahlawan dapat kita lihat sebagai sebuah grid berbentuk r baris dan c kolom.
Setiap petak dalam grid memiliki representasi yang berbeda-beda. Ada grid yang dapat dilewati dan ada grid yang tidak dapat dilewati, yang dilambangkan dengan karakter-karakter berikut:

- "M" : titik awal dari Master Gudako berada sebagai starting point dari petualangan Gudako
- "#" : batu besar yang tak dapat dilewati
- "S" : tempat untuk memanggil (summon) satu atau lebih pahlawan
- "D" : dungeon
- "." : petak yang dapat dilewati

## Petualangan Master Gudako

- Gudako memulai petualangan dari petak dengan karakter "M" (tanpa tanda kutip)
- Prioritas pengunjungan searah putaran jarum jam: atas, kanan, bawah, kiri
- Suatu petak tidak dapat dikunjungi lebih dari sekali.
- Jika pada suatu saat Gudako berada pada suatu petak dan di sekitarnya tidak terdapat petak baru untuk dikunjungi, maka Gudako akan mundur/kembali ke petak asal pengunjungan posisinya saat itu (backtrack)

## Petemuan Gudako dengan petak "S"

- Ketika menginjak petak berkarakter "S" Gudako dapat melakukan summoning pada pahlawan yang tertulis namanya pada petak tersebut
- Tipe pahlawan memiliki nama (unik), mana, level, senjata, dan kekuatan
- Tiap pahlawan sebelum mengikuti gudako memiliki level awal yaitu 1
- Level, kekuatan, dan mana adalah bilangan bulat non negatif
- Terdapat 2 senjata: pedang atau panah
- Ketika memasuki Dungeon kekuatan tiap pahlawan akan berubah bergantung dari tipe dungeon:
	- Pahlawan Pedang di Dungeon Pedang : kekuatan_asli
	- Pahlawan Pedang di Dungeon Panah	: floor(kekuatan_asli / 2)
	- Pahlawan Panah di Dungeon Pedang	: kekuatan_asli * 2
	- Pahlawan Panah di Dungeon Panah	: kekuatan_asli
- Pahlawan yang akan di-summon dimulai dari yang memiliki kekuatan paling tinggi
- Jika ada 2 atau lebih pahlawan dengan kekuatan sama, maka diurutkan berdasarkan mana yang paling rendah
- Jika mana pahlawan <= mana Gudako saat ini, maka pahlawan tersebut akan mengikutinya dan pada petak tersebut mana Gudako akan berkurang sejumlah mana pahlawan tersebut
- Jika mana pahlawan > mana Gudako, tidak akan ada lagi pahwalan yang mengikuti Gudako di petak tersebut
- NOTE: Mana Gudako selalu kembali ke jumlah maksimalnya tiap kali Gudako berpindah petak

## Masuk ke dalam Dungeon (simbol "D")

- Ketika Gudako menemui petak yang bersimbol “D” maka Gudako dapat mengajak maksimal sejumlah K pahlawan untuk masuk ke dalam Dungeon
- Tiap Dungeon memiliki kekuatan, jenis senjata, dan maksimal pahlawan yang boleh dibawa
- Jika jumlah kekuatan max Pahlawan yang dapat dibawa masuk Dungeon < kekuatan Dungeon, maka mereka memilih untuk tidak masuk Dungeon
- Jika >=, mereka masuk dan memenangkan battle di sana sehingga tiap pahlawan yang masuk akan naik level sejumlah level dari Dungeon sedangkan level Gudako akan bertambah sejumlah pahlawan yang ikut * level Dungeon
- Prioritas pahlawan yang diajak bertarung adalah pahlawan yang memiliki kekuatan di dalam Dungeon tersebut paling tinggi (kekuatan setelah dihitung tipe senjata di Dungeon)
- Jika ada 2 ata lebih pahlawan yang kekuatannya sama, maka diurutkan berdasarkan yang TERLAMA IKUT DENGAN GUDAKO (Jika Gudako mengambil 2 pahlawan atau lebih dalam 1 petak, maka yang muncul pertama di output adalah yang diambil pertama)

### Format Masukan

- Baris pertama	: 6 bilangan bulat positif Jp, Js, Jd, Ma, R, C yang secara berurut menyatakan jumlah pahlawan, jumlah tempat summon, jumlah dungeon, mana awal Gudako, panjang baris, panjang kolom dalam Dunia Pahlawan dan Dungeon
- Batasan:
	- 0 <= Jp <= 3000
	- 0 <= Js <= 2500
	- 0 <= Jd <= 2500
	- 0 <= Ma <= 1000000
	- 1 <= R <= 110
	- 1 <= C <= 110
- Baris 2 hingga Jp+2 				: informasi mengenai Pahlawan
	nama;mana;kekuatan;jenis senjata
- Baris Jp+2 hingga Jp+Js+2			: informasi mengenai tempat Summon
	r;c;nama Pahlawan1,nama Pahlawan2, ...
- Baris Jp+Js+2 hingga Jp+Js+Jd+3	: informasi mengenai Dungeon
	r;c;kekuatan;level;jenis senjata;max_pahlawan
- Baris selanjutnya hingga R+1 berisikan C buah karakter ASCII yang melambangkan dunia sesuai deskripsi di atas

Untuk semua input dijamin
- 1 ≤ r ≤ R, 1 ≤ c ≤ C
- Nama Pahlawan terdiri dari karakter alfanumerik (0-9A-Za-z) dan spasi saja
- Senjata pahlawan hanya 1 diantara pedang atau panah
- Senjata Musuh pada Dungeon hanya 1 diantara pedang atau panah
- Ada setidaknya 1 pahlawan di tiap petak “S”
- Hanya terdapat 1 karakter “M” pada Dunia Pahlawan dan Dungeon
- 0 <= kekuatan <= 2000000
- 0 <= mana <= 2000000
- 0 <= level <= 2000000
- 0 <= max_pahlawan <= 2000000

### Format Keluaran

- Setiap kali Gudako sampai ke petak "S", keluarkan baris:
	"r,c Pahlawan yang ikut: nama_pahlawan1,nama_pahlawan2, ..."
- Jika tidak ada pahlawan yang mengikuti Gudako, maka keluarkan baris:
	"r,c tidak ada pahlawan yang ikut"
- Setiap kali Gudako sampai ke petak "D", keluarkan baris:
	"r,c BATTLE, kekuatan: pow, pahlawan: nama_pahlawan1,nama_pahlawan2, ..."

- Jika kekuatan max yang dapat dibawa tidak cukup, maka tidak terjadi pertarungan sehingga keluarkan:
	"r,c RUN, kekuatan maksimal sejumlah: max_pos_pow"
	pow			: kekuatan jumlahan dari semua pahlawan yang ikut
	max_pos_pow	: kekuatan max yang mungkin untuk diikutkan
- Pada akhir petualangan Gudako, keluarkan baris:
	"Akhir petualangan Gudako"
	"Level Gudako: level"
	"Level Pahlawan:"
	"nama Pahlawan1 level Pahlawan1"
	"nama Pahlawan2 level Pahlawan2"
	...

	Diurutkan berdasarkan level tertinggi.
	Jika ada yang levelnya sama, diurutkan berdasarkan siapa yang lebih dulu mengikuti Gudako dalam petualangan ini.