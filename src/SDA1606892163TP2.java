import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.Comparator;
import java.lang.Math;
import java.util.Stack;

/**
 * Program untuk pengerjaan Tugas Pemrograman 2 mata kuliah Struktur Data dan Algoritma
 * @author Rico Putra Pradana - 1606892163 - SDA D 
 */
public class SDA1606892163TP2 {
	public static void main (String[] args) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		Permainan permainan = new Permainan(in);
		permainan.sol();
	}
}

/**
 * Class yang digunakan untuk menjalankan segala proses yang ada di program.
 * @author Rico Putra Pradana
 */
class Permainan {
	private BufferedReader in;
	private Map<String,Pahlawan> mapNamaPahlawan;
	private Map<String,ArrayList<Pahlawan>> mapTitikSummon;
	private Map<String,Dungeon> mapTitikDungeon;
	private int jmlBaris;
	private int jmlKolom;
	private Gudako gudako;
	private int startBaris;
	private int startKolom;
	
	public Permainan(BufferedReader in) {
		this.in = in;
		this.mapNamaPahlawan = new TreeMap<String,Pahlawan>();
		this.mapTitikSummon = new TreeMap<String,ArrayList<Pahlawan>>();
		this.mapTitikDungeon = new TreeMap<String,Dungeon>();
		this.jmlBaris = 0;
		this.jmlKolom = 0;
		this.startBaris = 0;
		this.startKolom = 0;
	}
	
	/**
	 * Method utama yang dijalankan oleh class Solution
	 * @throws IOException
	 */
	public void sol() throws IOException
	{
		String[] firstIn = in.readLine().split(" ");
		int jmlPahlawan = Integer.parseInt(firstIn[0]);
		int jmlSummon = Integer.parseInt(firstIn[1]);
		int jmlDungeon = Integer.parseInt(firstIn[2]);
		int manaGudako = Integer.parseInt(firstIn[3]);
		jmlBaris = Integer.parseInt(firstIn[4]);
		jmlKolom  = Integer.parseInt(firstIn[5]);
		
		gudako = new Gudako(manaGudako);
		
		for (int i = 0; i < jmlPahlawan; i++) {
			String[] secondIn = in.readLine().split(";");
			String nama = secondIn[0];
			int mana = Integer.parseInt(secondIn[1]);
			int kekuatan = Integer.parseInt(secondIn[2]);
			String senjata = secondIn[3];
			mapNamaPahlawan.put(nama, new Pahlawan(nama, mana, kekuatan, senjata));
		}
		
		for (int i = 0; i < jmlSummon; i++) {
			String[] thirdIn = in.readLine().split(";");
			int baris = Integer.parseInt(thirdIn[0]);
			int kolom = Integer.parseInt(thirdIn[1]);
			String[] pahlawan = thirdIn[2].split(",");
			
			mapTitikSummon.put(baris + "," + kolom, new ArrayList<Pahlawan>());
			for (int j = 0; j < pahlawan.length; j++) {
				String namaPahlawan = pahlawan[j];
				mapTitikSummon.get(baris + "," + kolom).add(mapNamaPahlawan.get(namaPahlawan));	
			}
		}
		
		for (int i = 0; i < jmlDungeon; i++) {
			String[] fourthIn = in.readLine().split(";");
			int baris = Integer.parseInt(fourthIn[0]);
			int kolom = Integer.parseInt(fourthIn[1]);
			int pow = Integer.parseInt(fourthIn[2]);
			int level = Integer.parseInt(fourthIn[3]);
			String jenisSenjata = fourthIn[4];
			int maxPahlawan = Integer.parseInt(fourthIn[5]);
			
			Dungeon dungeon = new Dungeon(pow, level, jenisSenjata, maxPahlawan);
			mapTitikDungeon.put(baris + "," + kolom, dungeon);
		}
		
		Character[][] petaDunia = new Character[jmlBaris][jmlKolom];
		
		for (int i = 0; i < jmlBaris; i++) {
			String peta = in.readLine();
			for (int j = 0; j < peta.length(); j++) {
				petaDunia[i][j] = peta.charAt(j);
				
				if (petaDunia[i][j] == 'M') {
					startBaris = i;
					startKolom = j;
				}
			}
		}
		jelajah(petaDunia, startBaris, startKolom);
		cetakAkhir();
	}
	
	/**
	 * Method untuk menjelajah petaDunia
	 * @param petaDunia - peta yang akan dijelajah
	 * @param baris - titik baris M atau titik baris awal
	 * @param kolom - titik kolom M atau titik kolom awal
	 */
	public void jelajah(Character[][] petaDunia, int baris, int kolom) {
		Stack<String> peta = new Stack<String>();
		peta.push(baris + "," + kolom);
		
		while (!peta.isEmpty()) {
			String[] titik = peta.pop().split(",");
			int titikBaris = Integer.parseInt(titik[0]); 
			int titikKolom = Integer.parseInt(titik[1]); 

			if (petaDunia[titikBaris][titikKolom] == 'S') {
				gudako.summon(mapTitikSummon.get((titikBaris+1) + "," + (titikKolom+1)), titikBaris, titikKolom);
			}
			else if (petaDunia[titikBaris][titikKolom] == 'D') {
				gudako.battle(mapTitikDungeon.get((titikBaris+1) + "," + (titikKolom+1)), titikBaris, titikKolom);
			}
	
			petaDunia[titikBaris][titikKolom] = '#';
			
			if (titikKolom != 0 && petaDunia[titikBaris][titikKolom-1] != '#') {
				peta.push(titikBaris + "," + (titikKolom-1));
				
			}
			if (titikBaris != jmlBaris - 1 && petaDunia[titikBaris+1][titikKolom] != '#') {
				peta.push((titikBaris+1) + "," + titikKolom);
			}
			if (titikKolom != jmlKolom - 1 && petaDunia[titikBaris][titikKolom + 1] != '#') {
				peta.push(titikBaris + "," + (titikKolom+1));
			}
			if (titikBaris != 0) {
				if (petaDunia[titikBaris-1][titikKolom] != '#') { 
					peta.push((titikBaris-1) + "," + titikKolom);
				}
			}
		}
	}
	
	/**
	 * Method untuk mencetak keluaran terakhir pada akhir petualangan Gudako
	 * @param gudako - Objek kelas Gudako
	 */
	public void cetakAkhir()
	{
		Collections.sort(gudako.getPahlawanGudako(), new ComparatorLevel());
		System.out.println("Akhir petualangan Gudako");
		System.out.println("Level Gudako: " + gudako.getLevelGudako());
		System.out.println("Level pahlawan:");
		
		for (int i = 0; i < gudako.getPahlawanGudako().size(); i++) {
			System.out.println(gudako.getPahlawanGudako().get(i).getNama() + ": " + gudako.getPahlawanGudako().get(i).getLevel());
		}
	}
}

/**
 * Class yang merepresentasikan Gudako sebagai tokoh utama di Dunia Pahlawan dan Gudako
 * @author Rico Putra Pradana
 */
class Gudako {
	private ArrayList<Pahlawan> pahlawanGudako;
	private int manaGudako;
	private int levelGudako;
	
	public Gudako(int mana) {
		this.pahlawanGudako = new ArrayList<Pahlawan>();
		this.manaGudako = mana;
		this.levelGudako =  1;
	}

	public ArrayList<Pahlawan> getPahlawanGudako() {
		return pahlawanGudako;
	}

	public void setPahlawanGudako(ArrayList<Pahlawan> pahlawanGudako) {
		this.pahlawanGudako = pahlawanGudako;
	}

	public int getLevelGudako() {
		return levelGudako;
	}

	public void setLevelGudako(int levelGudako) {
		this.levelGudako = levelGudako;
	}

	public int getManaGudako() {
		return manaGudako;
	}
	
	/**
	 * Method untuk summoning pahlawan yang terdapat di titik summon
	 * @param pahlawanSummon pahlawan-pahlawan yang terdapat pada titik summon
	 * @param baris titik summon
	 * @param kolom titik summon
	 */
	public void summon(ArrayList<Pahlawan> pahlawanSummon, int baris, int kolom)
	{
		Collections.sort(pahlawanSummon);
		int manaTemp = manaGudako;
		ArrayList<String> pahlawanYangIkut = new ArrayList<String>();
		
		for (int i = 0; i < pahlawanSummon.size(); i++) {
			if (pahlawanSummon.get(i).getMana() <= manaTemp) {
				pahlawanGudako.add(pahlawanSummon.get(i));
				manaTemp -= pahlawanSummon.get(i).getMana();
				pahlawanYangIkut.add(pahlawanSummon.get(i).getNama());
				pahlawanSummon.get(i).setLamaIkutGudako(pahlawanGudako.size());
			}
			else {
				break;
			}
		}		
		if (pahlawanYangIkut.size() > 0) {
			System.out.print((baris+1) + "," + (kolom+1) + " Pahlawan yang ikut:");
			for (int i = 0; i < pahlawanYangIkut.size(); i++) {
				if (i != pahlawanYangIkut.size() - 1) {
					System.out.print(pahlawanYangIkut.get(i) + ",");
				}
				else {
					System.out.println(pahlawanYangIkut.get(i));
				}
			}
		}
		else {
			System.out.println((baris+1) + "," + (kolom+1) + " tidak ada pahlawan yang ikut");
		}
	}
	
	/**
	 * Method untuk melangsungkan pertarungan di dalam dungeon
	 * @param dungeon - dungeon yang akan dijadikan tempat bertarung
	 * @param baris - titik dungeon
	 * @param kolom - titik dungeon
	 */
	public void battle(Dungeon dungeon, int baris, int kolom)
	{
		if (!pahlawanGudako.isEmpty()) {
			ArrayList<Pahlawan> pahlawanYangIkutBattle = new ArrayList<Pahlawan>();
			int maxKekuatanPahlawan = 0;
			
			if (dungeon.getJenisSenjata().equalsIgnoreCase("pedang")) {
				Collections.sort(pahlawanGudako, new FightingInSwordDungeon());
			} 
			else {
				Collections.sort(pahlawanGudako, new FightingInBowDungeon());
			}
			
			if (pahlawanGudako.size() > 0) {
				for (int i = 0; i < Math.min(dungeon.getMaxPahlawan(),pahlawanGudako.size()); i++) {
					pahlawanYangIkutBattle.add(pahlawanGudako.get(i));
					
					if (dungeon.getJenisSenjata().equalsIgnoreCase("pedang") && pahlawanGudako.get(i).getSenjata().equalsIgnoreCase("panah"))
						maxKekuatanPahlawan += (pahlawanGudako.get(i).getKekuatan() * 2);
					else if (dungeon.getJenisSenjata().equalsIgnoreCase("panah") && pahlawanGudako.get(i).getSenjata().equalsIgnoreCase("pedang"))
						maxKekuatanPahlawan += (pahlawanGudako.get(i).getKekuatan() / 2);
					else
						maxKekuatanPahlawan += pahlawanGudako.get(i).getKekuatan();
				}
			}
			
			if (maxKekuatanPahlawan >= dungeon.getKekuatan()) {
				for (int i = 0; i < pahlawanYangIkutBattle.size(); i++) {
					pahlawanYangIkutBattle.get(i).setLevel(pahlawanYangIkutBattle.get(i).getLevel() + dungeon.getLevel());
				}
				
				this.levelGudako += pahlawanYangIkutBattle.size() * dungeon.getLevel();
				
				System.out.print((baris+1) + "," + (kolom+1) + " BATTLE, kekuatan: " + maxKekuatanPahlawan + ", pahlawan: ");
				
				for (int i = 0; i < pahlawanYangIkutBattle.size(); i++) {
					if (i < pahlawanYangIkutBattle.size() - 1) {
						System.out.print(pahlawanYangIkutBattle.get(i).getNama() + ",");
					}
					else {
						System.out.println(pahlawanYangIkutBattle.get(i).getNama());
					}
				}
			}
			else {
				System.out.println((baris+1) + "," + (kolom+1) + " RUN, kekuatan maksimal sejumlah: " + maxKekuatanPahlawan);
			}
		}
		else {
			System.out.println((baris+1) + "," + (kolom+1) + " RUN, kekuatan maksimal sejumlah: 0");
		}
	}
	
}

/**
 * Class yang merupakan representasi dari Pahlawan yang ada di 
 * @author Rico Putra Pradana - 1606892163
 */
class Pahlawan implements Comparable<Pahlawan> {
	private String nama;
	private int mana;
	private int level;
	private String senjata;
	private int kekuatan;
	private int lamaIkutGudako;
	
	public Pahlawan(String nama, int mana, int kekuatan, String senjata) {
		this.nama = nama;
		this.mana = mana;
		this.level = 1;
		this.kekuatan = kekuatan;
		this.senjata = senjata;
		this.lamaIkutGudako = 0;
	}

	public String getNama() {
		return nama;
	}

	public int getMana() {
		return mana;
	}

	public int getLevel() {
		return level;
	}
	
	public void setLevel(int lev) {
		this.level = lev;
	}

	public String getSenjata() {
		return senjata;
	}

	public int getKekuatan() {
		return kekuatan;
	}
	
	public void setKekuatan(int pow) {
		this.kekuatan = pow;
	}
	
	public int getLamaIkutGudako() {
		return lamaIkutGudako;
	}
	
	public void setLamaIkutGudako(int time) {
		this.lamaIkutGudako = time;
	}
	
	/**
	 * Method untuk membandingkan setiap pahlawan dengan kekuatan (tinggi ke rendah)
	 * atau mana (rendah ke tinggi) sebagai pembanding
	 */
	public int compareTo(Pahlawan pah)
	{
		if (this.kekuatan == pah.getKekuatan()) {
			if (this.mana == pah.getMana()) {
				return this.nama.compareTo(pah.getNama());
			}
			else {
				return this.mana - pah.getMana();
			}
		}
		else {
			return pah.getKekuatan() - this.kekuatan;
		}
	}
}

/**
 * Kelas yang merepresentasikan objek Dungeon
 * @author Rico Putra Pradana
 */
class Dungeon {
	private int kekuatan;
	private int level;
	private String jenisSenjata;
	private int maxPahlawan;
	
	public Dungeon(int kekuatan, int level, String jenis, int max) {
		this.kekuatan = kekuatan;
		this.level = level;
		this.jenisSenjata = jenis;
		this.maxPahlawan = max;
	}


	public int getKekuatan() {
		return kekuatan;
	}

	public int getLevel() {
		return level;
	}

	public String getJenisSenjata() {
		return jenisSenjata;
	}

	public int getMaxPahlawan() {
		return maxPahlawan;
	}
}

/**
 * Class sebagai comparator saat para pahlawan bertarung di dungeon dengan tipe senjata pedang
 * @author Rico Putra Pradana
 */
class FightingInSwordDungeon implements Comparator<Pahlawan> {
	public int compare(Pahlawan a, Pahlawan b) 
	{
		int powA = a.getSenjata().equalsIgnoreCase("pedang") ? a.getKekuatan() : a.getKekuatan() * 2;
		int powB = b.getSenjata().equalsIgnoreCase("pedang") ? b.getKekuatan() : b.getKekuatan() * 2;
		
		if (powA == powB) {
			return a.getLamaIkutGudako() - b.getLamaIkutGudako();
		}
		else {
			return powB - powA;
		}
	}
}

/**
 * Class sebagai comparator saat para pahlawan bertarung di dungeon dengan tipe senjata panah 
 * @author Rico Putra Pradana
 */
class FightingInBowDungeon implements Comparator<Pahlawan> {
	public int compare(Pahlawan a, Pahlawan b)
	{
		int powA = a.getSenjata().equalsIgnoreCase("panah") ? a.getKekuatan() : a.getKekuatan() / 2;
		int powB = b.getSenjata().equalsIgnoreCase("panah") ? b.getKekuatan() : b.getKekuatan() / 2;

		if (powA == powB) {
			return a.getLamaIkutGudako() - b.getLamaIkutGudako();
		}
		else {
			return powB - powA;
		}
		
	}
}

/**
 * Class sebagai comparator dengan level pahlawan sebagai pembanding.
 * @author Rico Putra Pradana
 */
class ComparatorLevel implements Comparator<Pahlawan> {
	public int compare(Pahlawan a, Pahlawan b)
	{
		if (a.getLevel() == b.getLevel()) {
			return a.getLamaIkutGudako() - b.getLamaIkutGudako();
		}
		return b.getLevel() - a.getLevel();
	}
}